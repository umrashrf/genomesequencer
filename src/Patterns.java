import java.util.List;
import java.util.ArrayList;


public class Patterns {

	List<Pattern> store;

	public Patterns() {
		this.store = new ArrayList<>();
	}

	public Pattern get(Pattern p) {
		for (Pattern pattern: this.store) {
			if (p.genome.equals(pattern.genome)) {
				return pattern;
			}
		}
		return null;
	}

	public boolean exists(Pattern p) {
		if (this.get(p) != null) {
			return true;
		}
		return false;
	}

	public void add(Pattern p) {
		if (!this.exists(p)) {
			this.store.add(p);
		}
	}

	public int size() {
		return this.store.size();
	}
}
