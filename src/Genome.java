/*
 * Genome class is a collection of Nucleotides
 * We could use String but we need more control over each characters
 * for memory usage analysis
 */

import java.util.List;
import java.util.ArrayList;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;


public class Genome {

	List<Nucleotide> nucleotides;

	public Genome() {
		this.nucleotides = new ArrayList<Nucleotide>();
	}

	public void append(Nucleotide n) {
		this.nucleotides.add(n);
	}

	public Nucleotide get(int index) {
		return this.nucleotides.get(index);
	}

	public int size() {
		return this.nucleotides.size();
	}

	public String toString() {
		String rv = "";
		for (Nucleotide n: this.nucleotides) {
			rv += n.name();
		}
		return rv;
	}

	public boolean equals(Genome g) {
		return this.toString().equals(g.toString());
	}

	public static Genome fromFile(String filePath) throws IOException {

		FileReader fr = null;
		try {
			fr = new FileReader(filePath);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		if (fr != null) {
			BufferedReader br = new BufferedReader(fr);

			int in = br.read();

			Genome g = new Genome();

			while (in > -1 && in != 10) {
				String n = "";
				n += (char)in;
				g.append(Nucleotide.findByName(n));
				in = br.read();
			}

			fr.close();

			return g;
		}

		return null;
	}
}
