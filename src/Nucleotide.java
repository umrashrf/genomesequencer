/*
 * Nucleotides enum is to enforce specific genome characters
 * so we avoid random user input into the sequencer
 * because random input can generate unexpected results
 */

public enum Nucleotide {
	a, c, g, t;

	public static Nucleotide findByName(String name) {
		for (Nucleotide n: values()) {
			if (n.name().equals(name)) {
				return n;
			}
		}
		return null;
	}
}
