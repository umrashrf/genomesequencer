import java.io.IOException;

/*
 * Sequencer class is an entry point of the program
 */

public class Sequencer {

	Genome genome;
	int k; // k-mer

	public Sequencer(Genome genome, int k) {
		this.genome = genome;
		this.k = k;
	}

	public void run() {
		System.out.println("Input Genome: " + this.genome.toString());

		int genomeSize = this.genome.size();
		Patterns patterns = new Patterns();

		// p1 = position 1
		// p2 = position 2
		for (int p1 = 0, p2 = p1 + this.k - 1;
				p1 <= (genomeSize - this.k) && p2 < genomeSize;
				p1++, p2++) {

			Genome g = new Genome();

			// n = nucleotide
			for (int n = p1; n <= p2; n++) {
				g.append(this.genome.get(n));
			}

			Pattern p = new Pattern(g);

			if (patterns.exists(p)) {
				patterns.get(p).count++;
				patterns.get(p).positions.add(p1);
			} else {
				p.positions.add(p1);
				patterns.add(p);
			}
		}

		System.out.println("Total patterns found: " + patterns.size());

		for (Pattern p : patterns.store) {
			System.out.println(p.genome + ", " + p.count + ", " + p.positions.toString());
		}
	}

	public static void main(String args[]) {
		Genome g = null;
		try {
			g = Genome.fromFile("data/genome.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}

		Sequencer seq = new Sequencer(g, 2); // k = 2
		seq.run();
	}
}
