import java.util.List;
import java.util.ArrayList;


public class Pattern {

	int count;
	Genome genome;
	List<Integer> positions;

	public Pattern(Genome g) {
		this.count = 1;
		this.genome = g;
		this.positions = new ArrayList<>();
	}
}
